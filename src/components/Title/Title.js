import React from "react";

import classes from "./title.module.css";

function Title(props) {
  return (
    <h1
      className={`text-center rounded p-2 ${props.className} ${classes.title}`}
    >
      {props.children}
    </h1>
  );
}

export default Title;
