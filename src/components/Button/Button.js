import React from "react";

function Button(props) {
  return (
    <button
      style={props.isSelected ? { opacity: 1 } : { opacity: 0.5 }}
      className={`btn ${props.className}`}
      onClick={props.click}
    >
      {props.children}
    </button>
  );
}

export default Button;
