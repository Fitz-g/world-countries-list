import React from "react";
import Button from "../Button/Button";

function Filter(props) {
  return (
    <div className="d-flex mb-4 align-items-center">
      <div className="col-6">
        <Button
          className="text-white bg-primary me-1"
          click={() => props.handleClickFilter("all")}
          isSelected={props.selectedFiler === "all"}
        >
          Tous
        </Button>
        <Button
          className="text-white bg-primary me-1"
          click={() => props.handleClickFilter("Africa")}
          isSelected={props.selectedFiler === "Africa"}
        >
          Afrique
        </Button>
        <Button
          className="text-white bg-primary me-1"
          click={() => props.handleClickFilter("Asia")}
          isSelected={props.selectedFiler === "Asia"}
        >
          Asie
        </Button>
        <Button
          className="text-white bg-primary me-1"
          click={() => props.handleClickFilter("Americas")}
          isSelected={props.selectedFiler === "Americas"}
        >
          Amérique
        </Button>
        <Button
          className="text-white bg-primary me-1"
          click={() => props.handleClickFilter("Europe")}
          isSelected={props.selectedFiler === "Europe"}
        >
          Europe
        </Button>
        <Button
          className="text-white bg-primary"
          click={() => props.handleClickFilter("Oceania")}
          isSelected={props.selectedFiler === "Oceania"}
        >
          Océanie
        </Button>
      </div>
      <div className="col-6">
        Nombre de pays :{" "}
        <span className="badge bg-success">{props.countriesNumber}</span>
      </div>
    </div>
  );
}

export default Filter;
