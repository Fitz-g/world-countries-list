import React from "react";

function Countries(props) {
  return (
    <div className="col-md-5 mb-3 rounded border">
      <div className="d-flex gap-3 p-2">
        <img
          src={props.flag}
          alt={`${props.enName} flag`}
          style={{ width: "150px" }}
        />
        <div className="col-8">
          <h2 style={{ fontSize: "2rem" }}>Nom : {props.enName}</h2>
          <p>Capital : {props.capital}</p>
          <p className="mb-0">Région : {props.region}</p>
        </div>
      </div>
    </div>
  );
}

export default Countries;
