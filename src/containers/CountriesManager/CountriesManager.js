import React, { useEffect, useState } from "react";

import Title from "./../../components/Title/Title";
import Country from "../Country/Country";
import axios from "axios";
import Filter from "../../components/Filter/Filter";

function CountriesManager() {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selectedFitler, setSelectedFitler] = useState(null);

  const getRequest = (param) => {
    setLoading(true);
    let url = "";
    if (param === "all") {
      url = "all";
    } else {
      url = `region/${param}`;
    }
    axios
      .get(`https://restcountries.com/v2/${url}`)
      .then((response) => {
        let countriesList = response.data.map((country) => {
          return {
            enName: country.name,
            frName: country.translations.fr,
            flag: country.flag,
            capital: country.capital,
            region: country.region,
          };
        });
        setCountries(countriesList);
        setLoading(false);
        setSelectedFitler(param);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };

  useEffect(() => {
    getRequest("all");
  }, []);

  const handleClickFilter = (region) => {
    getRequest(region);
  };

  return (
    <>
      <Title className="my-5 bg-info text-white">LISTE DES PAYS DU MONDE</Title>

      <Filter
        handleClickFilter={handleClickFilter}
        selectedFiler={selectedFitler}
        countriesNumber={countries.length}
      />

      {loading ? (
        <div>Loading...</div>
      ) : (
        <div className="d-flex flex-wrap justify-content-between">
          {countries.map((country, index) => {
            return <Country key={index} {...country} />;
          })}
        </div>
      )}
      <div>Pagination</div>
    </>
  );
}

export default CountriesManager;
