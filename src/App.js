import "./App.css";
import CountriesManager from "./containers/CountriesManager/CountriesManager";

function App() {
  return (
    <div className="container">
      <CountriesManager />
    </div>
  );
}

export default App;
